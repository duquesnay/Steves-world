import PlaygroundSupport
import SpriteKit

public class DetectionZone : SKShapeNode {

    override init(){
        super.init()
    }

    public convenience init( rayon: CGFloat ){
        self.init()
        let diameter = rayon * 2
        self.path = CGPath(ellipseIn: CGRect(origin:CGPoint(x:-rayon,y:-rayon), size:CGSize(width: diameter, height: diameter)), transform: nil )
        self.isHidden = true
        self.physicsBody = defineImmaterialBody( radius: rayon )
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    func defineImmaterialBody( radius:CGFloat ) -> SKPhysicsBody {
        let stalactitesZoneBody = SKPhysicsBody(circleOfRadius: radius, center:CGPoint(x:0,y:0) )
        stalactitesZoneBody.isDynamic = false
        stalactitesZoneBody.affectedByGravity = false
        stalactitesZoneBody.collisionBitMask=0 // detection zone is immaterial, doesn't create collision
        return stalactitesZoneBody
    } 
} 


public class StalactitesChain : SKSpriteNode {
    
    public var stalactites = [Stalactite]()
    var chuteStalactitesAction : SKAction?
  
    public init() {
        super.init(texture:SKTexture(),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:CGSize(width: 0, height: 0))

        generateStalactitesAdjoinedToTheRight(count:8)
        createChuteAction()
    }
    
    func appendStalactiteAt(x: Double) -> Stalactite {
        let stalactite = Stalactite()
        stalactite.position=CGPoint(x: x, y: 0)
        stalactite.pinInPlace()
        stalactites.append( stalactite )
        addChild( stalactite )
        return stalactite
    }
    
    func generateStalactitesAdjoinedToTheRight(count: Int){
        self.anchorPoint = CGPoint(x:0,y:1.0) 
        var x = 0.0
        
        for _ in 1 ... count {
            let stalactite=appendStalactiteAt(x:x) // y doesn't change
            x += Double(stalactite.size.width) // get next right position.X
        }
    }
    
    public func setStalactitesCategoryBitMask(_ mask:UInt32 ){
        for s in stalactites {
            s.physicsBody!.categoryBitMask = mask
        }
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    public func detacher(){
        run( chuteStalactitesAction! )
    }
    
    func createChuteAction(){
        self.chuteStalactitesAction = SKAction.sequence([
            SKAction.run {
                let startTime = NSDate()
                NSDate().timeIntervalSince(startTime as Date)
                for stalactite in self.stalactites.enumerated() {
                    NSDate().timeIntervalSince(startTime as Date)
                    stalactite.element.waitThenFall(after: 0.3*Double(stalactite.offset))
                }
                NSDate().timeIntervalSince(startTime as Date)
            }
            ]
        )
    }
}
