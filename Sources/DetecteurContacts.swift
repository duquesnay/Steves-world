import PlaygroundSupport
import SpriteKit


func categorieDu(numero:Int) -> UInt32 {
    return UInt32(2<<numero)
}

/*
 @Todo: zone impenetrable
 @Todo: si toujours la, gene la descente
 */
public class DetecteurContacts : NSObject,SKPhysicsContactDelegate {
    
    var bodies = [SKPhysicsBody]()
    var triggers = [UInt32:SKAction]()
    
    func considere(_ o:SKNode ) -> SKPhysicsBody {
        let body = o.physicsBody!
        if !bodies.contains(body) {
            sensibiliseNouveauBody(body)
        }
        return body
    }
    
    func sensibiliseNouveauBody(_ body:SKPhysicsBody){
        let rang = bodies.count
        bodies.append(body)
        let masque = categorieDu(numero: rang)
        body.categoryBitMask = masque
    }
    
    func sensitivisedMask( of:SKPhysicsBody, to:SKPhysicsBody) -> UInt32 {
        return of.contactTestBitMask ^ to.categoryBitMask        
    }
    
    public func activeSiContact( detecteur:SKNode, declencheur:SKNode, action:SKAction ){
        let detectBody=considere(detecteur)
        let declenchBody=considere(declencheur)
        detectBody.contactTestBitMask = sensitivisedMask( of:detectBody, to:declenchBody)
        let contactMask=detectBody.categoryBitMask | declenchBody.categoryBitMask
        triggers[contactMask]=action
    }
    
    public func desactiveCollision( detecteur:SKNode, declencheur:SKNode, action:SKAction ){
        let detectBody=considere(detecteur)
        let declenchBody=considere(declencheur)
        detectBody.contactTestBitMask = sensitivisedMask( of:detectBody, to:declenchBody)
        let contactMask=detectBody.categoryBitMask | declenchBody.categoryBitMask
        triggers[contactMask]=action
    }
    
    public override init(){
        super.init()
    }
    
    public func declenchePeutEtre(_ contact: SKPhysicsContact) {
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        for (contactFilter,action) in triggers {
            if contactMask == contactFilter {
                contact.bodyA.node!.run(action)
            }
        }
    }
    
    public func didBegin(_ contact: SKPhysicsContact) {
        declenchePeutEtre(contact)
    }
}
