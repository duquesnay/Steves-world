import PlaygroundSupport
import SpriteKit


public class Murs : SKSpriteNode {

//    public convenience init(imageNamed:String, size:CGSize){
//        self.init(texture:SKTexture(imageNamed:imageNamed), size:size)
//    }
    
    public init(imageNamed: String, size: CGSize){
        super.init(texture: SKTexture(imageNamed: imageNamed),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:size)
          self.physicsBody=SKPhysicsBody(texture: texture!, alphaThreshold:0.5, size: size)
          self.physicsBody!.isDynamic = false
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

