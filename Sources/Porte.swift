import SpriteKit

public class Porte : SKSpriteNode {
    
    class Button : SKShapeNode {
        override init(){
            super.init();
            isUserInteractionEnabled=true
            strokeColor=#colorLiteral(red: 0.254901975393295, green: 0.274509817361832, blue: 0.301960796117783, alpha: 1.0)
        }
        public required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            (parent as! Porte).onButtonTriggered()
        }
    }
    var button:Button? = nil
    var isLifted = false

    public init(imageNamed: String, size: CGSize){
        super.init(texture: SKTexture(imageNamed: imageNamed),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:size)
        physicsBody=SKPhysicsBody(edgeFrom: CGPoint(x:0,y:0), to: CGPoint(x:size.width,y:size.height))
        self.anchorPoint=CGPoint(x:0.5,y:0)

        self.button = Button(rect: CGRect(x: -10, y: 30, width: size.width/2, height: size.height/8))
        self.addChild(button!)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func onButtonTriggered(){
        button!.fillColor = #colorLiteral(red: 0.274509817361832, green: 0.486274510622025, blue: 0.141176477074623, alpha: 1.0)
        lift()
    }
    
    public func lift(){
        guard !isLifted else { return }
        let liftAction = SKAction.move(by:CGVector(dx: 0, dy: self.size.height),duration:2.0)
        run(
            SKAction.sequence([
                liftAction,
                SKAction.wait(forDuration: 5.0),
                liftAction.reversed()
                ])
        )
        isLifted = true
    }
    
}

