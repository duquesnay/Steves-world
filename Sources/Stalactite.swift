import SpriteKit

// @todo use a dic/array of 32 possibilities; type is 2^index ; all but me is ~mask

public enum BodyType:UInt32 {
    case steve = 1
    case stalactiteZone = 2
    //case walls = 4
    //case doors = 8
    public func allButMe() -> UInt32 {
        switch self {
        case .steve : return BodyType.stalactiteZone.rawValue
        case .stalactiteZone : return BodyType.steve.rawValue
        }
    }
}


public class Stalactite : SKSpriteNode {
    
    public init(){
        super.init(texture:SKTexture(imageNamed:"Stalactite.png"),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:CGSize(width:50,height:50))
        let randomScale = CGFloat( Double(arc4random_uniform(7))/10 + 0.5 )
        self.size = self.size.applying(CGAffineTransform(scaleX:randomScale,y:randomScale))
        self.anchorPoint=CGPoint(x:0,y:1)
        
        physicsBody = createBodyTriangle()
        // @Todo relocatecollisionMask setting in scene composition via stalactitezone
        physicsBody?.collisionBitMask = BodyType.stalactiteZone.allButMe()
        physicsBody?.mass = 0.1
    }
    
    func createBodyTriangle() -> SKPhysicsBody {
        let trianglePath = CGMutablePath()
        trianglePath.move(to: CGPoint(x: -self.size.width/2*0.95, y: self.size.height*0.1))
        trianglePath.addLine(to: CGPoint( x: self.size.width/2*0.95, y: self.size.height*0.1))
        trianglePath.addLine(to: CGPoint( x: 0, y: -self.size.height*0.95))
        trianglePath.addLine(to: CGPoint( x: -self.size.width/2*0.95, y: self.size.height*0.1))
        return SKPhysicsBody( polygonFrom:trianglePath)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func waitThenFall(after duration:TimeInterval ){
        let waitNFall=SKAction.sequence([
            SKAction.wait(forDuration: duration),
            SKAction.run { self.fall() }
            ])
        run( waitNFall )
    }
    
    public func pinInPlace(){
    		physicsBody?.isDynamic = false
    		physicsBody?.pinned = true
    }
    
    public func fall(){
        physicsBody?.isDynamic = true
        physicsBody?.pinned = false
    }
}
