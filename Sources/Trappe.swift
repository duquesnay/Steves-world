import SpriteKit

public class Trappe : SKNode {
    
    let trapeGauche:SKSpriteNode
    let trapeDroite:SKSpriteNode
    var isOpened = false
    public override init(){
        self.trapeGauche = SKSpriteNode(texture:SKTexture(imageNamed:"trappe gauche.png"),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:CGSize(width:100,height:7))
        self.trapeDroite = SKSpriteNode(texture:SKTexture(imageNamed:"trappe droite.png"),color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:CGSize(width:100,height:4))
        super.init()
        miseEnPlaceTrappes()
    }
    
    func miseEnPlaceTrappes(){
        trapeGauche.position=CGPoint(
            x: -trapeGauche.size.width/2,
            y: 0)
        trapeGauche.physicsBody=SKPhysicsBody(rectangleOf: trapeGauche.size)
        trapeGauche.physicsBody?.isDynamic=false
        trapeGauche.physicsBody?.pinned=true
        addChild(trapeGauche)
        
        trapeDroite.physicsBody=SKPhysicsBody(rectangleOf: trapeDroite.size)
        trapeDroite.physicsBody?.isDynamic=false
        trapeDroite.position=CGPoint(
            x: trapeDroite.size.width/2,
            y: 0)
        trapeDroite.physicsBody?.pinned=true
        addChild(trapeDroite)
        self.isUserInteractionEnabled=true
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let largeurOuverture = 200
    func ouvrirTrappe() {
        guard !isOpened else { return }
        
        let tempsOuverture = 2.0
        let ouvertureTrappeGauche = SKAction.move(by:CGVector(dx: -largeurOuverture/2, dy: 0),duration:tempsOuverture)
        let ouvertureTrappeDroite = SKAction.move(by:CGVector(dx: largeurOuverture/2, dy: 0),duration:tempsOuverture)
        self.trapeGauche.run(ouvertureTrappeGauche)
        self.trapeDroite.run(ouvertureTrappeDroite)
        isOpened = true
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        ouvrirTrappe()
    }
}
