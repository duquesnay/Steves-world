import SpriteKit

public class Steve : SKSpriteNode {
    
    static private let stepSize=15.0
    static let step = SKAction.move(by:CGVector(dx:stepSize,dy:0),duration:0.2)
    
    public init(sprite: String, size:CGSize){
        let myTexture = SKTexture(imageNamed: sprite)
        super.init(texture: myTexture,color:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),size:size)
        
        physicsBody = SKPhysicsBody(rectangleOf: self.size.applying(CGAffineTransform(scaleX: 0.9, y: 0.9)))
        physicsBody?.mass = 0.4
        physicsBody?.restitution = 0.2
        physicsBody?.allowsRotation=false
        physicsBody?.usesPreciseCollisionDetection = true
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func moveRight(){
        if action(forKey: "right") != nil {
            return
        }
        run( SKAction.repeatForever( Steve.step ), withKey: "right" )
    }
    
    public func stop(){
        removeAction(forKey: "right")
    }
    
}
