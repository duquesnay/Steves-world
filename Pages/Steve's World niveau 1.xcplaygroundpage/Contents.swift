import PlaygroundSupport
import SpriteKit

/*
 @Todo: bug declenchement
 // necessite le bug des numeros pour marcher... sur les deux
 // necessite le premier declencheur pour que le second fonctionne
 */

let FRAME_SIZE = CGSize(width: 1024, height: 768)

let view = SKView(frame: CGRect(origin: CGPoint(x:0,y:0), size: FRAME_SIZE))
var scene: SKScene?

// ELEMENTS

let steve = Steve(sprite: "Steve.png",size: CGSize(width: 33, height: 70))
let porte = Porte(imageNamed: "Porte bouton.png", size:CGSize(width: 22, height: 100) )
let trappe = Trappe()
let murs = Murs(imageNamed: "structure niveau 2.png", size: CGSize(width:FRAME_SIZE.width,height:280))
let stalactites = StalactitesChain()
let zoneStalactites = DetectionZone( rayon:70 )

// ACTIONS

let chute = SKAction.sequence([
    SKAction.run{ 
        zoneStalactites.removeFromParent()
    },
    SKAction.wait(forDuration:1.5),
    SKAction.run {
        
        stalactites.detacher()
    }
    ]) 

let showAnnonceAction = SKAction.run{
    let annonce=SKLabelNode()
    annonce.fontColor = #colorLiteral(red: 0.745098054409027, green: 0.156862750649452, blue: 0.0745098069310188, alpha: 1.0)
    annonce.alpha = 1
    annonce.text = "IS DEADDDD!"
    annonce.position=CGPoint(x:scene!.frame.midX,y:scene!.frame.midY)
    scene!.addChild(annonce)
    annonce.run( SKAction.sequence([
            SKAction.wait(forDuration: 3.0),
            SKAction.removeFromParent()
    ]) )
        
    }
    
let newDetecteur = DetecteurContacts()
newDetecteur.activeSiContact( detecteur: zoneStalactites, declencheur: steve, action: chute)
newDetecteur.activeSiContact(detecteur: stalactites.stalactites[0], declencheur: steve, action: showAnnonceAction)
class SteveWorldScene1 : SKScene {
            
    override init(size: CGSize) {
        super.init(size:size)
        self.backgroundColor=#colorLiteral(red: 0.976470589637756, green: 0.850980401039124, blue: 0.549019634723663, alpha: 1.0)
    }
    
    override func didMove(to view: SKView) {
        self.installScene()
    }
    
    func installScene(){
        self.physicsBody = SKPhysicsBody(edgeLoopFrom:self.frame)
        self.physicsWorld.contactDelegate=newDetecteur
        self.physicsBody?.usesPreciseCollisionDetection = true
        self.physicsWorld.gravity.dy = -0.4
        
        
        addChild( murs )
        addChild( steve )
        addChild( porte )
        addChild( trappe )
        addChild( stalactites )
        addChild( zoneStalactites )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func shouldSteveMove( touches : Set<UITouch> ) -> Bool {
        let touch = touches.first
        let pointLocation = touch?.location(in: self)
        return pointLocation!.x > steve.position.x
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if shouldSteveMove(touches: touches) {
            steve.moveRight()
        } else {
            steve.stop()
        }
 
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        steve.stop()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if shouldSteveMove(touches: touches) {
            steve.moveRight()
        } else {
            steve.stop()
        }
    }
    
}

scene = SteveWorldScene1( size : FRAME_SIZE)

steve.position = CGPoint(x:20,y:600)

porte.position = CGPoint(x:200,y:390)
trappe.position = CGPoint(x:340,y:390)
murs.position = CGPoint(x:scene!.frame.midX,y:385)
stalactites.position=CGPoint(x:100,y:520)
zoneStalactites.position = CGPoint(x:110,y:520)
PlaygroundPage.current.liveView = view
view.presentScene(scene)
